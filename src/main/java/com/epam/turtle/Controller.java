package com.epam.turtle;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by NicTurtle on EPAM course Winter program 2022;
 */
public class Controller {
    /**
     * The Regex
     */
    // «mail@mail.com»
    public static final String REGEX_MAIL = "^([a-z0-9_-]+\\.)*[a-z0-9_-]+@[a-z0-9_-]+(\\.[a-z0-9_-]+)*\\.[a-z]{2,6}$";
    // «+38(095)555-55-55»
    public static final String REGEX_PHONE = "^\\+\\d{2}\\(\\d{3}\\)\\d{3}-\\d{2}-\\d{2}$";
    // «Иванов Иван Иванович»
    public static final String REGEX_FULL_NAME = "^([А-ЯA-Z]|[А-ЯA-Z][\\x27а-яa-z]{1,}|[А-ЯA-Z][\\x27а-яa-z]{1,}\\-([А-ЯA-Z][\\x27а-яa-z]{1,}|(оглы)|(кызы)))\\040[А-ЯA-Z][\\x27а-яa-z]{1,}(\\040[А-ЯA-Z][\\x27а-яa-z]{1,})?$";

    /**
     * Initialization of objects of classes Model and View
     */
    private final Model model;
    private final View view;

    /**
     * Constructor
     */
    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    /**
     * Starts filling in data for the contact book,
     * taking into account the validation of data entry
     */
    public void runQuestionnaire() {
        Scanner scanner = new Scanner(System.in);
        String fullName;

        view.printMessage(View.FIRST_MESSAGE);

        // Enter Full name
        do {
            view.printMessage(View.ENTER_LAST_NAME);
            model.setLastName(scanner.next());

            view.printMessage(View.ENTER_NAME);
            model.setName(scanner.next());

            view.printMessage(View.ENTER_PATRONYMIC);
            model.setPatronymic(scanner.next());

            fullName = model.getLastName() + " " + model.getName() + " " + model.getPatronymic();

            if (!regexTest(fullName, REGEX_FULL_NAME)) view.printMessage(View.WRONG_INPUT_DATA);

        } while (!regexTest(fullName, REGEX_FULL_NAME));

        // Enter phone number
        do {
            view.printMessage(View.ENTER_PHONE_NUMBER);
            model.setPhoneNumber(scanner.next());
            if (!regexTest(model.getPhoneNumber(), REGEX_PHONE)) view.printMessage(View.WRONG_INPUT_MOBILE_NUMBER);
        } while (!regexTest(model.getPhoneNumber(), REGEX_PHONE));

        // Enter email
        do {
            view.printMessage(View.ENTER_EMAIL);
            model.setEmailAddress(scanner.next());
            if (!regexTest(model.getEmailAddress(), REGEX_MAIL)) view.printMessage(View.WRONG_INPUT_DATA);
        } while (!regexTest(model.getEmailAddress(), REGEX_MAIL));

        //Create JSON file with this person on ContactBook
        model.addToContactBook(model);

        //Finish message
        view.printMessage(View.FINISH_MESSAGE);

    }

    /**
     * Checking the input string with a regular expression for correctness
     *
     * @param testString
     * @param regex
     * @return TRUE if input param is correct. FALSE if not correct.
     */
    public static boolean regexTest(String testString, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(testString);
        return matcher.matches();
    }

}
