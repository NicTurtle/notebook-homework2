package com.epam.turtle;

/**
 * Created by NicTurtle on EPAM course Winter program 2022;
 */
public class View {
    /**
     * Text's constants
     */
    public static final String FIRST_MESSAGE = "Please fill out the form.";
    public static final String  FINISH_MESSAGE = "***You have created a new contact in the contact book***";

    public static final String ENTER_LAST_NAME = "\nEnter last name: ";
    public static final String ENTER_NAME = "Enter name: ";
    public static final String ENTER_PATRONYMIC = "Enter patronymic: ";
    public static final String ENTER_PHONE_NUMBER = "Enter mobile phone number: ";
    public static final String ENTER_EMAIL = "Enter email address: ";

    public static final String WRONG_INPUT_DATA = "Wrong input! Repeat please! ";
    public static final String WRONG_INPUT_MOBILE_NUMBER = "Wrong input number! Repeat please! " +
            "\nSample of the correct number form - +38(095)555-55-55\n";

    /**
     * Print message to console
     * @param message
     */
    public void printMessage(String message){
        System.out.print(message);
    }

}
