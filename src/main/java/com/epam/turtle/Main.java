package com.epam.turtle;

/**
 * Created by NicTurtle on EPAM course Winter program 2022;
 */
public class Main {
    public static void main(String[] args) {
        // Initialization
        Model model = new Model();
        View view = new View();
        Controller controller = new Controller(model, view);

        controller.runQuestionnaire();
    }
}