package com.epam.turtle;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;

/**
 * Created by NicTurtle on EPAM course Winter program 2022;
 */
public class Model {
    /**
     * Variables containing contact information
     */
    private String lastName;
    private String name;
    private String patronymic;
    private String phoneNumber;
    private String emailAddress;

    /**
     * Get a Last Name
     *
     * @return Last Name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set the value of the Last Name variable
     *
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Get a Name
     *
     * @return Name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of the Name variable
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get a patronymic
     *
     * @return patronymic
     */
    public String getPatronymic() {
        return patronymic;
    }

    /**
     * Set the value of the Patronymic variable
     *
     * @param patronymic
     */
    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    /**
     * Get a phone number
     *
     * @return Phone number
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Set the value of the phone number variable
     *
     * @param phoneNumber
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Get a email address
     *
     * @return Email address
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Set the value of the email variable
     *
     * @param emailAddress
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * Combine last name, first name and patronymic
     *
     * @return Last name
     */
    private String getFullName() {
        return getLastName() + " " + getName() + " " + getPatronymic();
    }

    /**
     * Create a json file with the entered data in the contact book folder
     *
     * @param model
     */
    public void addToContactBook(Model model) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(new File("ContactBook/" + getFullName() + ".json"), model);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
